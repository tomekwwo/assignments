# This project was made only for educational purposes #

Aim of this project was to learn basic mechanics that have to be made for specific kinds of games.

### First part concentrates on First Person Shooter game mechanics: ###

* Walking and jumping in 3D space
* Aim weapon with mouse
* Shoot with a gun
* Additionally: slow motion on jump

**Controls:**

W / A / S / D - move character

Left Mouse Button - shoot

Move mouse - aim, look around

Space Bar - jump

Q / E - roll

### Second part is 2D car game. I wanted to make it look and feel realistic. Car consists of: ###

* Wheels connected with body using physics based suspension
* Engine script that handles ignition, gas, revolutions per minute and sounds
* Gearbox script responsible for transfering torque from engine to wheels and giving resistance to engine
* UI dashboard which shows RPM, speed in km/h and current gear

**Controls:**

Arrow Up / W - gas

Left Shift - Shift gear up

Left Control - Shift gear down

I - ignition