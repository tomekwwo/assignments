﻿using UnityEngine;

public class Speedometer : MonoBehaviour
{
    public GameObject pointer;
    public float maxRotation = -270;

    public void SetPointerRotation(float value, float maxValue)
    {
        float percentage = value / maxValue;
        pointer.transform.localRotation = Quaternion.Euler(0, 0, maxRotation * percentage);
    }
}
