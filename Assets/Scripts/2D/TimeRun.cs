﻿using UnityEngine;

public class TimeRun : MonoBehaviour
{
    private float timer = 0.0f;
    private bool start = false;

    private void Update()
    {
        if (start)
        {
            timer += Time.deltaTime;
            Debug.Log("Time: " + timer);
        }
    }

    public void StartCounting()
    {
        start = true;
        Debug.Log("Time run started!");
    }

    public void StopCounting()
    {
        start = false;
        Debug.Log("Finished in " + timer + " seconds");
    }
}
