﻿using UnityEngine;

public class Camera2D : MonoBehaviour
{
    public Transform target;
    public bool smooth = true;
    public float smoothing = 5f;

    private void LateUpdate()
    {
        if (target)
        {
            if (smooth)
            {
                Vector3 newPos = Vector3.Lerp(transform.position, target.position, Time.deltaTime * smoothing);
                newPos.z = -10;
                transform.position = newPos;
            }
            else
            {
                Vector3 newPos = target.position;
                newPos.z = -10;
                transform.position = newPos;
            }
        }
    }
}
