﻿using UnityEngine;

[RequireComponent(typeof(Car))]
public class Engine : MonoBehaviour
{
    public float maxRPM = 6000;
    public float idleRPM = 600;
    public float drag = 10;
    public float maxPower = 120;
    public float engineStarterInterval = 0.3f;
    public AudioClip ignitionSound;
    public AudioClip engineSound;

    [HideInInspector]
    public float RPM = 0;

    private float input;
    private float gasPressed = 0;
    private Car car;
    private bool ignition = false;
    private bool started = false;
    private float timer = 1f;
    private AudioSource audioSource;

    private void Start()
    {
        car = GetComponent<Car>();
        audioSource = GetComponent<AudioSource>();
        audioSource.clip = engineSound;
    }

    private void Update()
    {
        if (timer < engineStarterInterval)
            timer += Time.deltaTime;

        // get input
        input = Input.GetAxis("Vertical");

        // car ignition input
        if (Input.GetKeyDown(KeyCode.I))
        {
            if (started)
                started = false;
            else
            {
                ignition = true;
            }
        }
        if (Input.GetKeyDown(KeyCode.I))
        {
            // play ignition sound
            if (!started)
            {
                audioSource.Stop();
                audioSource.PlayOneShot(ignitionSound);
            }
        }
        if (Input.GetKeyUp(KeyCode.I))
        {
            ignition = false;
            if (!started)
                audioSource.Stop();
        }
    }

    private void FixedUpdate()
    {
        if (!started)
        {
            if (ignition)
            {
                // engine starter
                if (timer > engineStarterInterval)
                {
                    timer = 0.0f;
                    RPM += 150;
                }

                RPM += CurrentPower();

                if (RPM >= idleRPM)
                {
                    started = true;
                    // play engine sound
                    audioSource.Play();
                }
            }
        }
        else // if engine is started
        {
            if (Mathf.Abs(input) > Mathf.Epsilon)
            {
                gasPressed = Mathf.Abs(input);
            }
            else
            {
                // keep at least idle RPM
                if (RPM < idleRPM)
                {
                    gasPressed = 1;
                }
                else
                {
                    gasPressed = 0;
                }
            }

            if (RPM < Mathf.Epsilon)
            {
                started = false;
                audioSource.Stop();
            }

            if (RPM < maxRPM)
                AddRPM(CurrentPower() * gasPressed);
        }
        
        car.RefreshRPMLabel(RPM);
        car.gearbox.TransferTorque(RPM);
        // pitch 1 - 2
        SetEngineSound(1 + ((RPM - idleRPM) / maxRPM));
    }

    // calculate engine power with current RPM
    private float CurrentPower()
    {
        float rpmPercentage = RPM / maxRPM;
        return maxPower * rpmPercentage;
    }

    public void AddRPM(float quantity)
    {
        RPM += quantity;
        if (RPM < 0)
            RPM = 0;
    }

    private void SetEngineSound(float value)
    {
        audioSource.pitch = value;
    }
}
