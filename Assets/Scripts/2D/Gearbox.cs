﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Car))]
public class Gearbox : MonoBehaviour
{
    public float[] gears;
    public Rigidbody2D[] wheels;

    [HideInInspector]
    public float topSpeed;

    private int activeGear = 0;
    private Car car;
    private CircleCollider2D wheelCollider;

    private void Start()
    {
        car = GetComponent<Car>();

        if (gears.Length < 1)
        {
            Debug.LogError("Car gearbox has no gears!");
        }
        else
        {
            for (int i = 0; i < gears.Length; i++)
            {
                // default gear is neutral
                if (gears[i] == 0)
                {
                    activeGear = i;
                }
            }
            car.RefreshGearLabel(GearLabel());
        }

        if (wheels.Length < 1)
            Debug.LogError("Gearbox has no wheels to propel!");

        CalculateTopSpeed();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.LeftShift))
            ShiftGearUp();
        else if (Input.GetKeyDown(KeyCode.LeftControl))
            ShiftGearDown();
    }

    public void TransferTorque(float RPM)
    {
        foreach (Rigidbody2D wheel in wheels)
        {
            // limit wheel angular velocity
            if (gears[activeGear] != 0)
            {
                float maxWheelSpeed = RPM * gears[activeGear];
                // increase wheel velocity and decrease engine RPM
                // 1000 - 1200 = -200 1400 - 1200 = 200 maxSpeed - speed
                //float velocityDiff = maxWheelSpeed - wheel.angularVelocity;
                //wheel.angularVelocity += velocityDiff;
                //car.engine.AddRPM(-velocityDiff * gears[activeGear]);

                wheel.angularVelocity = Mathf.Lerp(wheel.angularVelocity, maxWheelSpeed, 0.25f);
                car.engine.RPM = Mathf.Lerp(car.engine.RPM, car.engine.RPM - Mathf.Abs(wheel.angularVelocity - maxWheelSpeed), 0.25f);
                //Debug.Log("RPM " + Mathf.RoundToInt(car.engine.RPM) + "/" + Mathf.RoundToInt(car.engine.RPM - Mathf.Abs(wheel.angularVelocity - maxWheelSpeed)) + 
                //    " wheel " + Mathf.RoundToInt(wheel.angularVelocity) + "/" + Mathf.RoundToInt(maxWheelSpeed));
                if (car.engine.RPM < 0)
                    car.engine.RPM = 0;
            }
        }

        if (gears[activeGear] == 0)
            car.engine.AddRPM(-car.engine.drag);
    }

    public void ShiftGearUp()
    {
        if (activeGear < (gears.Length - 1))
        {
            activeGear++;
            car.RefreshGearLabel(GearLabel());
        }
    }

    public void ShiftGearDown()
    {
        if (activeGear > 0)
        {
            activeGear--;
            car.RefreshGearLabel(GearLabel());
        }
    }

    private string GearLabel()
    {
        if (gears[activeGear] < 0)
            return "R";
        else if (gears[activeGear] == 0)
            return "N";
        else
            return (activeGear - 1).ToString();
    }

    private void CalculateTopSpeed()
    {
        ArrayList gearList = new ArrayList(gears);
        gearList.Sort();
        float topGear = (float) gearList[gears.Length - 1];

        wheelCollider = wheels[0].GetComponent<CircleCollider2D>();
        float wheelLength = wheelCollider.radius * 2 * Mathf.PI;
        topSpeed = Mathf.Abs((topGear * car.engine.maxRPM) / 360) * wheelLength * 3.6f;
    }

    public float CalculateWheelSpeed()
    {
        float wheelLength = wheelCollider.radius * 2 * Mathf.PI;
        return Mathf.Abs(wheels[0].angularVelocity / 360) * wheelLength * 3.6f;
    }
}
