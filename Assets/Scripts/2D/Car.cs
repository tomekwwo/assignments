﻿using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Engine))]
[RequireComponent(typeof(Gearbox))]
[RequireComponent(typeof(Rigidbody2D))]
public class Car : MonoBehaviour
{
    public Text gearText;
    public Text RPMText;
    public Text speedText;
    public Speedometer tachometer;
    public Speedometer speedometer;

    [HideInInspector]
    public Engine engine;
    [HideInInspector]
    public Gearbox gearbox;
    [HideInInspector]
    public Rigidbody2D rb;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        engine = GetComponent<Engine>();
        gearbox = GetComponent<Gearbox>();
    }

    private void Update()
    {
        RefreshDashboard();
    }

    private void RefreshDashboard()
    {
        RefreshSpeedLabel();
    }

    public void RefreshSpeedLabel()
    {
        if (speedText)
        {
            // actual speed of car body in km/h
            float speed = rb.velocity.magnitude * 3.6f;
            // car speed calculated using wheel rotational speed in km/h
            //float speed = gearbox.CalculateWheelSpeed();
            speedText.text = Mathf.RoundToInt(speed).ToString();
            speedometer.SetPointerRotation(gearbox.CalculateWheelSpeed(), gearbox.topSpeed);
        }
    }

    public void RefreshRPMLabel(float RPM)
    {
        if (RPMText)
        {
            RPMText.text = Mathf.RoundToInt(RPM).ToString();
            tachometer.SetPointerRotation(RPM, engine.maxRPM);
        }
    }

    public void RefreshGearLabel(string gear)
    {
        if (gearText)
        {
            gearText.text = gear;
        }
    }
}
