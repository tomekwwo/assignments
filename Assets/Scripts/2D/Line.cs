﻿using UnityEngine;

public class Line : MonoBehaviour
{
    public TimeRun timeRun;
    public bool start = true;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            if (start)
                timeRun.StartCounting();
            else
                timeRun.StopCounting();
        }
    }
}
