﻿using UnityEngine;

public enum WheelDrive
{
    FWD, RWD, AWD
}

public class CarController : MonoBehaviour
{
    public WheelJoint2D rearWheel;
    public WheelJoint2D frontWheel;
    public WheelDrive wheelDrive = WheelDrive.RWD;
    public float maxSpeed = 2000;
    public float acceleration = 1;
    public bool controlTorque = false;
    public float engineTorque = 300;
    
    private float horizontal = 0.0f;
    private Rigidbody2D rigid;
    private float timer = 0.0f;
    private bool speedAchieved = false;
    private Rigidbody2D rearWheelRigid;
    private Rigidbody2D frontWheelRigid;

    private void Start()
    {
        rigid = GetComponent<Rigidbody2D>();

        // set wheel drive
        if (!controlTorque)
        {
            if (wheelDrive == WheelDrive.RWD)
            {
                rearWheel.useMotor = true;
                frontWheel.useMotor = false;
            }
            else if (wheelDrive == WheelDrive.FWD)
            {
                rearWheel.useMotor = false;
                frontWheel.useMotor = true;
            }
            else
            {
                rearWheel.useMotor = true;
                frontWheel.useMotor = true;
            }
        }

        rearWheelRigid = rearWheel.connectedBody;
        frontWheelRigid = frontWheel.connectedBody;
    }

    private void Update()
    {
        horizontal = Input.GetAxis("Horizontal");
        Debug.Log("Wheel angular velocity: " + rearWheelRigid.angularVelocity);
    }

    private void FixedUpdate()
    {
        if (wheelDrive == WheelDrive.AWD)
        {
            if (controlTorque)
            {
                AddTorque(frontWheelRigid);
                AddTorque(rearWheelRigid);
            }
            else
            {
                SetMotorSpeed(rearWheel);
                SetMotorSpeed(frontWheel);
            }
        }
        else if (wheelDrive == WheelDrive.RWD)
        {
            if (controlTorque)
            {
                AddTorque(rearWheelRigid);
            }
            else
                SetMotorSpeed(rearWheel);
        }
        else
        {
            if (controlTorque)
            {
                AddTorque(frontWheelRigid);
            }
            else
                SetMotorSpeed(frontWheel);
        }

        if (rigid.velocity.magnitude < 6 && !speedAchieved)
        {
            timer += Time.deltaTime;
            //Debug.Log("Velocity: " + rigid.velocity.magnitude);
        }
        else if (rigid.velocity.magnitude >= 6 && !speedAchieved)
        {
            //Debug.Log("Speed achieved in " + timer + " seconds");
            speedAchieved = true;
        }
    }

    private void SetMotorSpeed(WheelJoint2D wheel)
    {
        JointMotor2D motor = wheel.motor;
        
        if (horizontal == 0f)
            motor.motorSpeed -= motor.motorSpeed * 0.01f;

        motor.motorSpeed = Mathf.Clamp(motor.motorSpeed - (horizontal * acceleration), -maxSpeed, maxSpeed);

        wheel.motor = motor;
    }

    private void AddTorque(Rigidbody2D wheel)
    {
        wheel.AddTorque(-engineTorque * horizontal, ForceMode2D.Force);
    }
}
