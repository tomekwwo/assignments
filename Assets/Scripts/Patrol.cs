﻿using UnityEngine;

public class Patrol : MonoBehaviour
{
    public Transform item;
    public Transform P1;
    public Transform P2;

    private Transform target;

    private void Start()
    {
        item.position = P1.position;
        target = P2;
    }

    private void Update()
    {
        if (Vector3.Distance(item.position, target.position) > float.Epsilon)
        {
            item.position = Vector3.MoveTowards(item.position, target.position, 5 * Time.deltaTime);
        }
        else
        {
            if (target == P1)
                target = P2;
            else
                target = P1;
        }
    }
}
