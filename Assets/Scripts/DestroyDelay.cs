﻿using UnityEngine;

public class DestroyDelay : MonoBehaviour
{
    public float delay = 3f;

    private void Start()
    {
        if (delay >= 0)
        {
            Destroy(gameObject, delay);
        }
    }
}
