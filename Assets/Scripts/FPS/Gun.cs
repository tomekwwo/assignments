﻿using UnityEngine;

public class Gun : MonoBehaviour
{
    public Transform barrelEnd;
    public float range = 100f;
    public LayerMask shootableMask;
    public AudioClip[] gunShotSounds;
    public float shotsPerSecond = 1;
    public float recoil = 1.0f;
    public bool canSwitchModes = false;
    public Sprite[] bulletHoleSprites;
    public GameObject bulletHolePrefab;

    private LineRenderer gunLine;
    private Ray ray;
    private RaycastHit hit;
    private AudioSource audioSource;
    private float shotRatio;
    private float timer = 0.0f;
    private bool automatic = false;
    private Transform cameraTransform;

    private void Start()
    {
        gunLine = barrelEnd.GetComponent<LineRenderer>();
        gunLine.SetWidth(0.01f, 0.01f);
        audioSource = GetComponent<AudioSource>();
        shotRatio = 1 / shotsPerSecond;
        cameraTransform = GetComponentInParent<Camera>().transform;
    }

    private void Update()
    {
        timer += Time.deltaTime;

        if (Input.GetKeyDown(KeyCode.Z) && canSwitchModes)
        {
            automatic = !automatic;
        }

        ray = new Ray(barrelEnd.position, barrelEnd.forward);

        gunLine.enabled = true;
        gunLine.SetPosition(0, barrelEnd.position);

        Shoot(ray);

        ShowLaser(ray);
    }

    private void Shoot(Ray ray)
    {
        bool shootMode;
        if (automatic)
        {
            shootMode = Input.GetMouseButton(0);
        }
        else
            shootMode = Input.GetMouseButtonDown(0);

        if (shootMode && timer >= shotRatio)
        {
            cameraTransform.Rotate(new Vector3(-recoil, 0, 0));
            audioSource.PlayOneShot(gunShotSounds[Random.Range(0, gunShotSounds.Length - 1)]);
            if (Physics.Raycast(ray, out hit, range, shootableMask))
            {
                // instantiate a bullet hole
                GameObject bulletHole = (GameObject) Instantiate(bulletHolePrefab, hit.point, Quaternion.FromToRotation(Vector3.up, hit.normal));
                bulletHole.GetComponentInChildren<SpriteRenderer>().sprite = bulletHoleSprites[Random.Range(0, bulletHoleSprites.Length)];
                bulletHole.transform.Translate(Vector3.up * 0.001f);
                bulletHole.transform.SetParent(hit.transform);
                Destroy(bulletHole, 5);
            }
            timer = 0.0f;
        }
    }

    private void ShowLaser(Ray ray)
    {
        if (Physics.Raycast(ray, out hit, range, shootableMask))
        {
            gunLine.SetPosition(1, hit.point);
        }
        else
        {
            gunLine.SetPosition(1, ray.origin + ray.direction * range);
        }
    }
}
