﻿using System.Collections;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float moveSpeed = 0.1f;
    public float jumpSpeed = 5;
    public float minAngle = -60f;
    public float maxAngle = 60f;
    public float maxTimeSlow = 0.5f;
    public AudioClip[] walkingSounds;
    public AudioClip jumpSound;
    public AudioClip landSound;
    
    private Vector3 input = Vector3.zero;
    private Rigidbody rb;
    private bool jump = false;
    private Transform cameraTransform;
    private Ray ray;
    private RaycastHit hit;
    private int contactWithObjects = 0;
    private Coroutine slowMotion;
    private AudioSource audioSource;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        cameraTransform = gameObject.GetComponentInChildren<Camera>().transform;
        audioSource = GetComponent<AudioSource>();
    } 

    private void Update()
    {
        GetKeyboardInput();
    }

    private void FixedUpdate()
    {
        MovePlayer();
        RotatePlayer();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!other.isTrigger && other.tag != "Player")
        {
            // just hit the ground
            if (contactWithObjects == 0)
            {
                audioSource.PlayOneShot(landSound);
            }
            contactWithObjects++;
        }

        if (contactWithObjects > 0)
        {
            if (slowMotion != null)
                StopCoroutine(slowMotion);
            slowMotion = StartCoroutine(GameController.SmoothSlowMotion(1.0f, 6));
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (!other.isTrigger && other.tag != "Player")
        {
            contactWithObjects--;
        }

        if (contactWithObjects == 0)
        {
            if (slowMotion != null)
                StopCoroutine(slowMotion);
            slowMotion = StartCoroutine(GameController.SmoothSlowMotion(maxTimeSlow, 6));
        }
    }

    private void MovePlayer()
    {
        // create movement vector
        Vector3 moveVector = transform.forward * input.z; // add forward movement
        moveVector += transform.right * input.x; // add horizontal movement
        moveVector *= moveSpeed; // regulate speed
        
        if (IsGrounded())
        {
            if (moveVector != Vector3.zero)
            {
                if (!audioSource.isPlaying)
                {
                    // play random walking sound
                    audioSource.PlayOneShot(walkingSounds[Random.Range(0, walkingSounds.Length - 1)]);
                }
            }

            if (jump)
            {
                audioSource.Stop();
                audioSource.PlayOneShot(jumpSound);
                rb.velocity = new Vector3(rb.velocity.x, jumpSpeed, rb.velocity.z);
            }
        }
        jump = false;

        // move player
        rb.MovePosition(rb.position + moveVector * Time.deltaTime);
    }

    private IEnumerator Roll(float degrees)
    {
        float rot = 0;

        while (rot < Mathf.Abs(degrees))
        {
            rot += Mathf.Abs(degrees) * Time.deltaTime;
            rb.MoveRotation(Quaternion.Euler(new Vector3(
                rb.rotation.eulerAngles.x,
                rb.rotation.eulerAngles.y,
                rb.rotation.eulerAngles.z + degrees * Time.deltaTime
                )));
            yield return null;
        }

        rb.rotation = Quaternion.Euler(rb.rotation.eulerAngles.x, rb.rotation.eulerAngles.y, 0);
    }

    private void RotatePlayer()
    {
        // create rotation vector
        Vector3 rotationVector = new Vector3(
            rb.rotation.eulerAngles.x,
            rb.rotation.eulerAngles.y + Input.GetAxis("Mouse X") * GameController.instance.settings.mouseSensitivity,
            rb.rotation.eulerAngles.z
            );

        // rotate player
        rb.MoveRotation(Quaternion.Euler(rotationVector));

        // camera rotation
        cameraTransform.eulerAngles = new Vector3(
            ClampAngle(cameraTransform.rotation.eulerAngles.x - Input.GetAxis("Mouse Y") * GameController.instance.settings.mouseSensitivity, minAngle, maxAngle),
            cameraTransform.rotation.eulerAngles.y,
            cameraTransform.rotation.eulerAngles.z
            );
    }

    private void GetKeyboardInput()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            jump = true;
        }

        if (Input.GetKeyDown(KeyCode.Q))
        {
            jump = true;
            StartCoroutine(Roll(360));
        }
        else if (Input.GetKeyDown(KeyCode.E))
        {
            jump = true;
            StartCoroutine(Roll(-360));
        }

        input = new Vector3(Input.GetAxis("Horizontal"), 0f, Input.GetAxis("Vertical"));
        input = Vector3.ClampMagnitude(input, 1.0f);
    }

    private bool IsGrounded()
    {
        if (contactWithObjects > 0)
            return true;
        else
            return false;
    }

    private float ClampAngle(float angle, float min, float max)
    {
        if (angle < 90 || angle > 270)
        {
            if (angle > 180)
                angle -= 360;
            if (max > 180)
                max -= 360;
            if (min > 180)
                min -= 360;
        }

        angle = Mathf.Clamp(angle, min, max);
        if (angle < 0)
            angle += 360;

        return angle;
    }
}
