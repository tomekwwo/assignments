﻿using System;
using System.Collections;
using UnityEngine;

[Serializable]
public class GameSettings
{
    public float mouseSensitivity;

    public GameSettings(float mouseSensitivity)
    {
        this.mouseSensitivity = mouseSensitivity;
    }
}

public class GameController : MonoBehaviour
{
    public GameSettings settings;
    public static GameController instance;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else
            Destroy(gameObject);
        if (instance != this) return;

        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (!Cursor.visible)
            {
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
            }
            else
            {
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;
            }
        }
    }

    public static void SetSlowMotion(float timeScale)
    {
        Time.timeScale = timeScale;
    }

    public static IEnumerator SmoothSlowMotion(float timeScale, float smoothness)
    {
        while (Mathf.Abs(Time.timeScale - timeScale) > float.Epsilon)
        {
            Time.timeScale = Mathf.Lerp(Time.timeScale, timeScale, Time.deltaTime * smoothness);
            Time.fixedDeltaTime = 0.02f * Time.timeScale;
            yield return null;
        }

        Time.timeScale = timeScale;
        Time.fixedDeltaTime = 0.02f * Time.timeScale;
    }
}